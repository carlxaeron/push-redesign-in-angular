import { PushAngularPage } from './app.po';

describe('push-angular App', () => {
  let page: PushAngularPage;

  beforeEach(() => {
    page = new PushAngularPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
