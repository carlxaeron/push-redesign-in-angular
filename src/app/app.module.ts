import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { HeaderComponent } from './header/header.component';
import { ContentsComponent } from './contents/contents.component';
import { ContentsHomeComponent } from './contents/contents-home/contents-home.component';
import { PhotosComponent } from './contents/photos/photos.component';
import { AdminComponent } from './contents/admin/admin.component';
import { ErrorComponent } from './contents/error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentsComponent,
    ContentsHomeComponent,
    PhotosComponent,
    AdminComponent,
    ErrorComponent,
  ],
  imports: [
    BrowserModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
