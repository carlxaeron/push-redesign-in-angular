import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentsHomeComponent } from './contents/contents-home/contents-home.component';
import { PhotosComponent } from './contents/photos/photos.component';
import { AdminComponent } from './contents/admin/admin.component';
import { ErrorComponent } from './contents/error/error.component';

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ContentsHomeComponent },
  { path: 'photos', component: PhotosComponent },
  { path: 'admin', component: AdminComponent },
  { path: '**', component: ErrorComponent },
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);