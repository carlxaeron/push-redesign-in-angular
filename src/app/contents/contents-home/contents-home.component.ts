import { Component, OnInit } from '@angular/core';
import { ContentsComponent } from './../contents.component';

import { HomeService } from './home.service';

@Component({
  selector: 'push-container',
  templateUrl: './contents-home.component.html',
  styleUrls: ['./contents-home.component.sass'],
  providers: [HomeService]
})
export class ContentsHomeComponent implements OnInit {

  headlines: object[] = [
    {
      title: 'Angular Test 1'
    },
    {
      title: 'Angular Test 2'
    },
    {
      title: 'Angular Test 3'
    },
    {
      title: 'Angular Test 4'
    },
  ];

  constructor(
    homeService: HomeService,
    contentsComponent: ContentsComponent
  ) { 
    this.headlines = homeService.getHeadlines();
    contentsComponent.page = 'homepage';
  }

  ngOnInit() {
    
  }

}
