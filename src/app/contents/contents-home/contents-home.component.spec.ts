import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentsHomeComponent } from './contents-home.component';

describe('ContentsHomeComponent', () => {
  let component: ContentsHomeComponent;
  let fixture: ComponentFixture<ContentsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
