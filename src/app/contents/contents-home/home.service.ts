export class HomeService {

  headlines: object[] = [
    {
      title: 'Angular Test 1'
    },
    {
      title: 'Angular Test 2'
    },
    {
      title: 'Angular Test 3'
    },
    {
      title: 'Angular Test 4'
    },
  ];

  getHeadlines() : object[] {
    return this.headlines;
  }

  setHeadlines(input) {
    this.headlines.push(input);
  }
}