import { Component, OnInit } from '@angular/core';

import { ContentsComponent } from './../contents.component';

@Component({
  selector: 'push-container',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.sass']
})
export class ErrorComponent implements OnInit {

  constructor(contentsCompononent: ContentsComponent) { 
    contentsCompononent.page = 'error-page';
  }

  ngOnInit() {
  }

}
