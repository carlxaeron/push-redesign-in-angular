import { Component, OnInit } from '@angular/core';

import { ContentsComponent } from './../contents.component';

@Component({
  selector: 'push-container',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.sass']
})
export class PhotosComponent implements OnInit {

  constructor(contentsCompononent: ContentsComponent) { 
    contentsCompononent.page = 'landing-pages';
  }

  ngOnInit() {
  }

}
