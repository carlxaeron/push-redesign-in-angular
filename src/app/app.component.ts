import { Component } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { ContentsComponent } from './contents/contents.component';

@Component({
  selector: 'push-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'app';
}
