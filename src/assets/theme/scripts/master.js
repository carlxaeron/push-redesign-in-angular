/*!
 * [PUSH Redesign 2017]
 * ABS-CBN Corporation 2017
 */

function getMeta(name){

	var _metas = document.getElementsByTagName('meta');

	for(var i=0; i<_metas.length; i++){
		if(_metas[i].getAttribute('name') == name || _metas[i].getAttribute('property')){
			return _metas[i].getAttribute('content');
		}
	}
	return false;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var _FB_ID = getMeta('fb:app_id');
var PATH_ASSETS = typeof PATH_ASSETS == 'undefined' ? '//assets.abs-cbn.com/push/test/' : PATH_ASSETS;

_FB_ID = typeof getUrlParameter('test') !== 'undefined' ? getUrlParameter('test') : _FB_ID;

var jQuery = window.$ = window.jQuery = require("jquery");
require('slick-carousel');
require('select2');
require('placeholder.js');
require('tooltipster');
require('picturefill');
require('sticky-kit/dist/sticky-kit.js');
require('jscroll');
require('dotdotdot');
// require('vminpoly');
require('historyjs/scripts/uncompressed/history.js');
require('historyjs/scripts/uncompressed/history.adapter.jquery.js');
var loadImage = require('load-img');
var browser = require('detect-browser');
// console.log(browser.name);
// console.log(browser.version);
// require('angular');

	var _TEST = false;
	if(typeof(TEST) !== 'undefined'){
		_TEST = true;
	}

var ABSCBN = (function() {
	// 'use strict';
	

	var getUrl = window.location;
	var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
	var BACK_FORWARD = false;

	function init() {
		//- $(document).ready(function(){
		// var UNINAV_CONFIG = {
		// 		type: "default", //required 
		// 		uninav: true, //required
		// 		oldsearch: true,
		// 		domainKey: 'push'
		// };
		// //UNINAV
		// if(typeof $().uninav == 'function') $('header').uninav(UNINAV_CONFIG);
		// //- });

    window.LoadMoreStoriesTest = function(){
      var _lmst = require('pug-loader!./../_pug/partials/_home_li.pug');
      var _return = '';

      for(i=1;i<=10;i++){
        _return += _lmst({
          SAMPLES: 'theme/images/_samples/',
          i: i,
          config:{
            devtest: false
          }
        })
      }
      return _return;
    }

    var PushSkinningTimer;
		var PushSkinning = function PushSkinning(){
			var _psc = $('#push-skinning-container');
			var _pclar = $('#push-container-left-and-right');
			var _psm = $('#push-skinning-mobile');
			var _psmP = _psm.find('picture source');

			_psm.length > 0 && 
			_psc.hasClass('with-skin') == false && 
			_psc.addClass('with-skin') &&
			_psm.append('<div class="push-picture"></div>');

			setTimeout(function(){
				PushHTMLFixes({
					detectHomeSlider:true
				});
				_psm.length > 0 && $('.no-push-headlines > #push-home-top-container').first().addClass('with-skin');
			},200);

			if(_psmP.length == 3){
				_psmP.each(function(k,v){
					switch(k){
						case 0:
							typeof $(v).data('init') == 'undefined' && 
							$(v).attr('media','(max-width: 568px)') && 
							$('.push-picture').append('<img src="'+$(v).attr('srcset')+'" data-size="1">') &&
							$(v).data('init',true);
							break;
						case 1:
							typeof $(v).data('init') == 'undefined' && 
							$(v).attr('media','(max-width: 959px) and (min-width: 569px)') && 
							$('.push-picture').append('<img src="'+$(v).attr('srcset')+'" data-size="2">') &&
							$(v).data('init',true);
							break;
						case 2:
							typeof $(v).data('init') == 'undefined' && 
							$(v).attr('media','(min-width: 960px)') && 
							$('.push-picture').append('<img src="'+$(v).attr('srcset')+'" data-size="3">') &&
							$(v).data('init',true);
							break;
					}
				});
				_psm.addClass('picture-fixed');
			}

			function clearSkinning(){
				_psc.attr('style','').prop('style','');
				_pclar.attr('style','').prop('style','');
			}

			if(_psm.length > 0 && $(window).width() > 960){
        if(PushSkinningTimer) clearInterval(PushSkinningTimer);
        PushSkinningTimer = setInterval(function(){
				  var _pscT = parseInt(_psc.css('padding-top').replace('px',''));

          _psc.height(_pclar.outerHeight());

          _pclar.css({top:_pscT});
        },300);
			}
			else clearSkinning();

			if(_psm.length > 0) {
				_psm.unbind().click(function(e){
					var _dURL = $(this).data('url');

					if(typeof _dURL !== 'undefined' && _dURL != false){
						window.open(_dURL,'_blank');
					}
					else {
						console.info('No link found');
					}
				});
			}

		}

		var loadmorebtnTimer = false;
		//HOMEPAGE
		var PushHeadlines = function PushHeadlines() {
			$('.push-headlines ul').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots:true,
				prevArrow: '<div class="ph-prev"></div>',
				nextArrow: '<div class="ph-next"></div>',
			});
		}
		$('header.uninav-push.uninav-default .header-nav > ul > li').click(function(){
			PushHeadlines()
		});
		if($('#homepage').length) {
			PushHeadlines();
			//LOAD MORE
			// window.LoadMoreStories = function() {
			// 	if(_TEST){
			// 		var ajax = $.ajax('theme/scripts/json/test1.json');
			// 		ajax.success(function(data){
			// 			var data = data.d;
			// 			console.log(data)
			// 		});
			// 	}
			// }
			PushSkinning();

			$('.load-more-btn').click(function(e){
				loadmorebtnTimer = setInterval(function(){
					if($(e.currentTarget).hasClass('loading') == false){
						PushSkinning();
						PushHTMLFixes({
							pushArticlesInfoFixes: true
						});
            $(window).trigger('scroll');
						clearInterval(loadmorebtnTimer);
					}
				},300);
			});
		}

		var SlickImageStart = false;

		var SlickImage = function SlickImage(d) {			
			var _d = $.extend({
				target: '.push-gallery',
				btnTarget: ''
			}, d);

			PushHTMLFixes({
				galleryFixes: true
			});

			//BIG IMAGES
			$('.pg-big ul', _d.target).slick({
				// arrows: false,
				prevArrow: '<div class="pg-prev"></div>',
				nextArrow: '<div class="pg-next"></div>',
				fade:true,
				asNavFor: _d.btnTarget+'.push-gallery .pg-thumbnails ul',
			});
			//THUMBNAILS
			$('.pg-thumbnails ul', _d.target).slick({
				prevArrow: '<div class="pg-prev"></div>',
				nextArrow: '<div class="pg-next"></div>',
				slidesToShow: 5,
				slidesToScroll: 1,
				asNavFor: _d.btnTarget+'.push-gallery .pg-big ul',
				focusOnSelect: true,
				responsive: [{
					breakpoint: 769,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1,
						arrows: false
					}
				}]
			});

			var imgIndex = getUrlParameter('img');

			SlickImageStart === false && 
			typeof imgIndex !== 'undefined' && 
			$('.pg-big ul i[data-nodeid="'+imgIndex+'"]').length &&
			$('.pg-big ul, .pg-thumbnails ul', _d.target).slick('slickGoTo',$('.pg-big ul i[data-nodeid="'+imgIndex+'"]').parents('li').first().index());
			SlickImageStart = true;

			PushHTMLFixes({
				slickFixes: true
			});
		}

		var PushClickGTM = new function(_d){
			function clickGTM(){
				function dataLayerPush(d){
					dataLayer.push({
						'event':'gtm.js',
						gtm:{
							start:new Date().getTime()
						}
					});
					var _d = $.extend({
						pageView:true
					},d);
					dataLayer.push({'pageView':_d.pageView});
					// if(typeof dataLayer !== 'undefined') console.log(dataLayer);
				}

				if(typeof dataLayer == 'undefined'){
					(function (w, d, s, l, i) {
						w[l] = w[l] || []; w[l].push({
						'gtm.start':
						new Date().getTime(), event: 'gtm.js'
						}); var f = d.getElementsByTagName(s)[0],
						j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
						'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
					})(window, document, 'script', 'dataLayer', typeof _GTM_ID2 !== 'undefined' ? _GTM_ID2 : _GTM_ID);
				}
				dataLayerPush();
			}
			$('body').on('click', '.pg-thumbnails .slick-slide', function() {
				clickGTM()
			});
			$('body').on('click', '.pg-prev, .pg-next', function(e) {
				var _href = $(e.currentTarget).parents('.push-gallery').first().find('.pg-big .slick-active').data('href');
				var _url = $(e.currentTarget).parents('article').first().data('URL');
				var _title = $(e.currentTarget).parents('article').first().find('#title').text();
				History.pushState({}, _title + ' | Push', getPathFromUrl(_url) + _href);
				clickGTM()
			});
		}

		//HTML FIXES
		var PushHTMLFixes = function(d){
			var conf = $.extend({
				innerPagesSocialMedia: false,
				slickFixes: false,
				galleryFixes: false,
				pushArticlesInfoFixes: false,
				skinningFixes: false,
				mainMinHFixes:false,
				wrapTagandCommentFix:false,
				searchMinHFix:false,
				buttonIEFix:false,
				insertAds:false,
				newHeaderFix:false,
				detectHomeSlider:false,
				spritesheetFixes:false
			},d);
			if(conf.innerPagesSocialMedia === true){
				$('main.main#inner-pages .contents article').each(function(k,v){
					$(v).find('.social-save-container').each(function(k2,v2){
						k2 === 1 && $(v2).find('.social').hasClass('bottom-social') == false && $(v2).find('.social').addClass('bottom-social');
					});
				});
			}
			if(conf.slickFixes === true){
				$('.pg-next').each(function(k,v){
					$(v).hasClass('slick-next') == false && $(v).addClass('slick-next');
				});
				$('.pg-prev').each(function(k,v){
					$(v).hasClass('slick-prev') == false && $(v).addClass('slick-prev');
				});
			}
			if(conf.galleryFixes === true) {
				$('.pg-big ul li, .pg-thumbnails ul li').each(function(k,v){
					if(typeof $(v).data('href') == 'undefined') {
						// if($(v).index() === 0) $(v).data('href','');
						if(typeof $(v).find('[class*="icon-"]').first().data('nodeid') !== 'undefined') $(v).data('href','?img='+$(v).find('[class*="icon-"]').first().data('nodeid'));
						// else $(v).data('href','?imgIndex='+$(v).index());
						// console.log(v,$(v).index(),$(v).data())
					}
				});
			}
			if(conf.pushArticlesInfoFixes === true){
				$('main.main:not(#special-page):not(#search-page) .push-articles ul li').each(function(k,v){
					var _pai = {};
					_pai.t = $('.push-articles-img',v);
					_pai.h = _pai.t.outerHeight(true);
					var _painfo = {};
					_painfo.t = $('.push-articles-info-container .push-articles-info',v);
					_painfo.h = _painfo.t.outerHeight(true);
					var _rs = {};
					_rs.t = $('#responses-shares',_painfo.t);
					_rs.h = _rs.t.outerHeight(true);

					_painfo.t.css({'min-height':(_pai.h - _rs.h)});
				});
			}
			if(conf.skinningFixes){
				var _t = $('main.main#homepage .contents #push-skinning-container.with-skin #push-skinning-mobile');
				_t.css({
					'background-image':'url('+$('source[media="(min-width: 960px)"]',_t).attr('srcset')+')'
				}).addClass('fixed');
			}
			if(conf.mainMinHFixes){
				$('main.main').css({'min-height':$(window).height() - $('footer').height() - ($('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header').height())});
			}
			if(conf.wrapTagandCommentFix){
				$('.push-infinite-scroll > article, .push-infinite-scroll > .jscroll-inner').each(function(k,v){
					$('.push-tag',v).length == false && $('#tags',v).before('<div class="push-tag"></div>');
				});
			}
			if(conf.searchMinHFix){
				$('main.main#search-page .contents .push-articles ul li').each(function(k,v){
					var _pai = {};
					_pai.t = $('.push-articles-img',v);
					_pai.h = _pai.t.height();
					var _paic = {};
					_paic.t = $('.push-articles-info-container',v);
					_paic.t.css({
						'min-height':_pai.h
					});
				});
			}
			if(conf.buttonIEFix){
				function msieversion() {

					var ua = window.navigator.userAgent;
					var msie = ua.indexOf("MSIE ");

					if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
					{
						// alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
						return true;
					}
					else  // If another browser, return 0
					{
						// alert('otherbrowser');
					}

					return false;
				}
				if(msieversion()) {
					$('a > button').each(function(k,v){
							typeof $(v).data('buttonInit') == 'undefined' && $(v).click(function(e){
								e.preventDefault();
								window.location.href = $(e.currentTarget).parent().attr('href');
							}) && $(v).data('buttonInit',true);
					});
				}
			}
			if(conf.insertAds) {
				if($('.article-data').length && typeof InsertAds != 'undefined'){
					if(typeof loadBreakerdAd == 'undefined') window.loadBreakerdAd = function loadBreakerdAd(){
						console.info('loadBreakerdAd() function does not exist!');
					}
					InsertAds();
					$('.article-data.active').first().removeClass('active');
					$('.article-data .ads-block.active').first().removeClass('active');
				} else if(typeof loadBreakerdAd != 'undefined') {
					loadBreakerdAd();
					$('.ads-block.active').first().removeClass('active');
				}
			}
			if(conf.newHeaderFix) {
				var _t = $('header.uninav-push.uninav-default .header-text');
				// _t.find('img').length == false && _t.html('<img src="'+PATH_ASSETS+'theme/images/push-logo-text.png" alt="ABS-CBN">');
				_t.find('span').length == false && _t.html('<span id="push-text-logo">');
			}
			if(conf.detectHomeSlider) {
				var _t = $('main.main#homepage .contents');
				_t.find('.push-headlines').length == false && _t.addClass('no-push-headlines');
			}
			if(conf.spritesheetFixes) {
				$('.social').each(function(k,v){
					// $(v).parents('.push-gallery').first().length == false && $(v).find('i').each(function(k2,v2){
					$(v).find('i').each(function(k2,v2){
						$(v2).find('img').remove();
					}) && $(v).addClass('social-fixed');
				});
			}
		}
		PushHTMLFixes({
			innerPagesSocialMedia: true,
			pushArticlesInfoFixes: true,
			skinningFixes: true,
			mainMinHFixes:true,
			searchMinHFix:true,
			buttonIEFix:true,
			insertAds:true,
			newHeaderFix:true,
			detectHomeSlider:true,
			spritesheetFixes:true
		});

		//INNER COMMENTS
		var PushInnerComments = new function(){
			$(document.body).on('click','article .comments#button',function(){
				$(this).toggleClass('open');
				$(this).parents('article').first().find('[id*="commentsDiv-"]').slideToggle('fast');
			});
		}
		//ELLIPSIS
		var PushEllipsis = function PushEllipsis(){
			var _hlsnt = $('main.main#homepage > .contents .push-lsn .push-lsn-info-container .push-lsn-info #title');
			var _p_hlsnt = _hlsnt.parent().height();
			var _d_hlsnt = _hlsnt.parent().find('#source-date').height() + 10;
			_hlsnt.dotdotdot({
				height: _p_hlsnt - _d_hlsnt,
				watch: true				
			});

			// var _lpt = $('main.main#landing-pages > .contents .push-container-left .push-articles ul li .push-articles-info-container .push-articles-info');
			// _lpt.each(function(k,v){
			// 	var _lpt = $(v);
			// 	var _lptH = _lpt.height();
			// 	var _lptH2 = _lpt.find('#person-date').outerHeight(true) + _lpt.find('#responses-shares').outerHeight(true) + 20;
			// 	console.log(_lptH, _lpt.find('#person-date').outerHeight(true), _lpt.find('#responses-shares').first().height())
			// 	_lpt.find('#title').dotdotdot({
			// 		height: _lptH - (_lptH2),
			// 		watch: true
			// 	});
			// });

			var _splp = $('main.main#special-page .contents #latest-photos #latest-photos-contents > .push-articles');
			_splp.find('#title').dotdotdot({
				height: 75,
				watch: true
			});

			var _t1pai = $('main.main .push-articles.type-1 ul li');
			_t1pai.find('#title').dotdotdot({
				watch: true,
				height: _t1pai.find('.push-articles-img').height()
			});
		}
		var PushTooltip = new function(){
			var _tt = $('.tooltip_templates .tooltip_content .push-articles ul');
			_tt.parent().append('<div class="loader"><img src="'+ PATH_ASSETS +'theme/images/loader.gif"/></div>');
		}

		PushEllipsis();
		//INNER PAGES
		if($('#inner-pages').length) {
			SlickImage({});
			PushHTMLFixes({
				wrapTagandCommentFix:true
			});
		}
		//SPECIAL PAGE
		if($('#special-page').length) {
			var _pfsul = $('.push-fresh-scoops ul')
			.after('<div id="mobile-nav"><span>1</span> of <span>'+$('.push-fresh-scoops ul li').length+'</span> <div id="mobile-nav-arrows"></div></div>')
			.slick({
				infinite: true,
				slidesToShow: 3,
				slidesToScroll: 3,
				dots:true,
				prevArrow: '<div class="pfs-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
				nextArrow: '<div class="pfs-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
				appendArrows: '#mobile-nav-arrows',
				responsive: [{
					breakpoint: 960,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: false
					}
				}]
			});
			_pfsul.on('afterChange',function(slick, currentSlide){
				$('#mobile-nav span').first().text(currentSlide.currentSlide + 1);
			});
			//NAV TO SELECTION
			$('.header-container:nth-child(1) nav li a').each(function(k,v){
				if($(this).parents('.header-container').first().find('nav.mobile').length == false) {
					$(this).parents('.header-container').first().append('<nav class="mobile"><select></select></nav>');
				}
				$('nav.mobile select').append('<option data-target="'+$(v).data('target')+'">'+$(v).text()+'</option>');
			});
			//SCROLL TO LINK
			function scrollTo(target) {
				if($("[target='"+target+"']").length == false) console.error('target="' + target + '" cant find.');
				else{
					$('html,body').animate({
						scrollTop: $("[target='"+target+"']").offset().top - $('header.uninav-push .header-fixed').height()},
					'slow');
				}
			}
			$('[data-target]').click(function(e){
				e.preventDefault();
				scrollTo($(e.target).data('target'));
			}).parent().change(function(e){
				e.preventDefault();
				scrollTo($(e.target).find(':selected').data('target'));
			});

			var _pb1 = $('main.main#special-page .contents #latest-photos .push-background');
			$('main.main#special-page .contents #latest-photos').css({
				'background-image':'url('+(_pb1.attr('data-cfsrc') ? _pb1.attr('data-cfsrc') : _pb1.attr('src'))+')'
			})
			.addClass('fixed');
			_pb1 = $('main.main#special-page .contents .header-container:nth-child(2) .push-background');
			$('main.main#special-page .contents .header-container:nth-child(2)').css({
				'background-image':'url('+(_pb1.attr('data-cfsrc') ? _pb1.attr('data-cfsrc') : _pb1.attr('src'))+')'
			})
			.addClass('fixed');
		}
		//SEARCH PAGE
		if($('#search-page').length) {
			if($('#results-input > i').length == false) {
				$('#results-input').append('<i>x</i>');
			}
			// X Button
			$('#results-input > i').click(function(e){
				$(e.target).parent().find('input').val('').focus();
			});
			$('#results-input input').keydown(function(e){
				if (e.keyCode == 13) {
					e.preventDefault();
					if(typeof Search == 'function') Search();
					else window.location.href = baseUrl + 'search?searchText=' + encodeURIComponent($(this).val());
				}
			});
		}
		//GLOBAL SEARCH PAGE
		var PushSearch = new function(){
			function clickI(target) {
				target.trigger('click');				
			}
			$('header.uninav-push.uninav-default .header-social .search').click(function(e){
				clickI($(e.target).find('i'));
			});
			
			$('header.uninav-push.uninav-default .header-social .search span').click(function(e){
				clickI($(e.target).parents('.search').first().find('i'));
			});

			$('header.uninav-header .search-container div.search-form').wrap('<form class="uninav_search_mobile">');

			var _SearchInput = 'input[type="textbox"], input[type="search"], input[name="search"]';

            $('html')
                .on('submit', 'header form', function(e) {
                    e.preventDefault();
                    var _val = $(_SearchInput, $(e.target)).val();
                    _val = $.trim(_val);
                    _val = encodeURIComponent(_val);
                    // window.location.href = baseUrl + ($(e.currentTarget).parents('.test').length ? "search-all.html?" : "search/") + _val;
                    uninav_search(e, _val);
                })
                .on('click', 'header form .search-button', function(e) {
                    var _val = $(_SearchInput, $(e.target).parents('form').first()).val();
                    _val = $.trim(_val);
                    _val = encodeURIComponent(_val);
                    uninav_search(e, _val);
                });

            $(window).keydown(function(e) {
                if (e.keyCode == 13 && $(e.target).parents('.uninav_search_mobile').length) {
                    e.preventDefault();
                    var _val = $(_SearchInput, $(e.target).parents('form').first()).val();
                    _val = $.trim(_val);
                    _val = encodeURIComponent(_val);
                    uninav_search(e, _val);
                    return false;
                }
            });

            function uninav_search(e, text) {
                window.location.href = baseUrl + ($(e.currentTarget).parents('.test').length ? "search-all.html?" : "search?searchText=") + text;
                // window.location.href = baseUrl;
            }
		}
		//INFINITE SCROLL
		var PIS_next = false;
		var _PIS_next = [];
		var _PIS_next2 = [];
		var _currentUrl = window.location.href;
		var PushHistory = function PushHistory() {
			function cleanURL(url){
				url = url.replace(/([^:]\/)\/+/g, "$1");
				url = url.split(/[?#]/)[0];
				return url;
			}

			var _t = $('.push-infinite-scroll');

			if(_PIS_next2.length == false){
				_PIS_next2.push(window.location.href);
				$.each(PIS_next,function(a,b){
					_PIS_next2.push(cleanURL(baseUrl+b));
					// _PIS_next.push(baseUrl+b);
				});
			}

			function addDataURL(d){
				var conf = $.extend({
					target:false
				},d);
				var _t2 = conf.target ? conf.target : _t;
				_t2.each(function(k,v){
					// if(typeof $(v).find('article').first().data('URL') == 'undefined'){
						$(v).find('article').first().data('URL',_PIS_next2[k]);
					// }
				});
			}
			addDataURL();

			$(window).scroll(function(){
				var _t = $('.push-infinite-scroll');
				var _ispT = $('.push-container-scroll');
     
				if(_PIS_next.length == false){
					_PIS_next.push(window.location.href);
					$.each(PIS_next,function(a,b){
						_PIS_next.push(cleanURL(baseUrl+b));
						// _PIS_next.push(baseUrl+b);
					});
				}

        var _setTitle = false;
				var _setURL = false;

				addDataURL({
					target:_t
				});

				_t.each(function(k,v){
					var _tlH = $(v).offset().top;
					if($(v).parents('.jscroll-added').length) {
						_tlH = _tlH - (parseInt($(v).parents('.jscroll-added').css('padding-top')));
					}
					var _wT = $(window).scrollTop() + $('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header').height();
					var _tUrl = _PIS_next[k];
					if(_wT > _tlH) {
						_setURL = _tUrl;
						_setTitle = $(v).find('#title').first().text();
					}
				});
				if(_setURL === false) _setURL = _PIS_next[0];
				// console.log(_t.length,_PIS_next,_currentUrl,_setURL,'not-equals?:' + (_currentUrl != _setURL),BACK_FORWARD)
				if(_setURL != _currentUrl){
					History.pushState({state:_currentUrl}, _setTitle + ' | Push', _setURL);
					_currentUrl = _setURL;
				}
				// BACK_FORWARD = false; 
			});
			// $(window).on('popstate',function(event) {
			// 	console.log('popstate',event)
            //     if(event.originalEvent != null) {
            //         var State = History.getState();
			// 		console.log('STATE:'+State);
            //         BACK_FORWARD = true;
            //     }
            // });
		}
		var PushInfiniteScroll = function PushInfiniteScroll() {
			var _t = $('.push-infinite-scroll');
			if(PIS_next == false){
				PIS_next = _next = $('.next[data-urls]').data('urls').split(';');
			}
			_next = _next.filter(function(v){return v!==''});

			if(typeof _next[_t.length - 1] !== 'undefined'){
				$('.push-infinite-scroll article #next, .push-infinite-scroll article .next').append('<a href="' + _next[$('.push-infinite-scroll').length - 1] + '"></a>');
			}

			function callBack(){
				var arr = new Array();
				var _t = $('.jscroll-added').last();
				var _g = _t.find('.push-gallery');
				var _img = _t.find('#article-img, #article-video');
				var _gl = _g.length;
				var _comments = _t.find('[id*="commentsDiv-"]').data();
				if(_gl) {
					var _target = 'push-gallery-' + _gl;
					_g.addClass(_target);
					arr.btnTarget = '.'+_target;
				}
				if(_img) {
					arr.articleImg = _img;					
				}
				if(_comments) {
					arr.comments = _comments;
				}
				arr.target = _t;
				return arr;
			}

			_t.jscroll({
				loadingHtml: '<div class="loader"><img src="'+PATH_ASSETS+'theme/images/loader.gif"/></div>',
				padding: 200,
				nextSelector: '#next a, .next a',
				contentSelector: '.push-infinite-scroll',
				// autoTriggerUntil: 4,
				callback:function(){
					PushInfiniteScroll();
					PushSocialMedia();

					var cb = callBack();

					// PushStickyKit({recalc:true});
					SlickImage({
						target: cb.target,
						btnTarget: cb.btnTarget,
					});
					PushAspectRatios({
						target: cb.articleImg
					});
					PushBreadCrumbs({
						target: cb.target
					});
					PushInitComments({
						categoryID:cb.comments.cid,
						streamID:cb.comments.streamid,
						containerID:cb.comments.containerid
					});
					PushHTMLFixes({
						innerPagesSocialMedia: true,
						wrapTagandCommentFix: true,
						insertAds: true,
						spritesheetFixes: true
					});

					setInterval(function(){
						//BRIGHTCOVE
						$('.video-js').each(function(k,v){
							if(_BC_SCRIPT === false){
								_BC_SCRIPT = '<script src="'+$('.video-js + script').first().attr('src')+'"></script>';
							}

							if(typeof $(v).data('initBC') == 'undefined'){
								$(_BC_SCRIPT).appendTo(document.body);
								$(v).data('initBC',true);
							}
						});
						//IG SCRIPT
						typeof window.instgrm == 'undefined' && $('<script async="" defer="" src="//platform.instagram.com/en_US/embeds.js"></script>').appendTo(document.body);
						typeof window.instgrm != 'undefined' && typeof window.instgrm.Embeds != 'undefined' && typeof window.instgrm.Embeds.process != 'undefined' && window.instgrm.Embeds.process();
					},2500);
				}
			});
		};
		var _BC_SCRIPT = false;
		if($('.push-infinite-scroll').length){
			PushInfiniteScroll();
			PushHistory();
		}

		//STICKY KIT
		var PushStickyKit = function PushStickyKit(d) {
			// var _d = $.extend({
			// 	recalc: false
			// },d);

			// if(_d.recalc === true) {
			// 	$('.push-container-scroll > .push-container-right').trigger('sticky_kit:recalc');
			// }
			// else {
			// 	if($(window).width() >= 960){
			// 		// $('.push-container-scroll > .push-container-left, .push-container-scroll > .push-container-right').stick_in_parent({recalc_every: 1});	
			// 		$('.push-container-scroll > .push-container-right').stick_in_parent({
			// 			recalc_every: 1,
			// 			parent:'section'
			// 		});	
			// 	}
			// 	else {
			// 		$('.push-container-scroll > .push-container-right').trigger("sticky_kit:detach");	
			// 	}
			// }
			// $(window).unbind('scroll','scrollSticky');
			
			// var _d = $.extend({
			// 	parent: false,
			// 	target:'aside.push-container-right'
			// },d);

			// var _target = {};
			// _target.t = $(_d.target);
			// _target.top = _target.t.offset().top;
			// _target.h = _target.t.height();
			// _target.l = _target.t.offset().left;
			// _target.w = _target.t.width();
			// _target.t.find('.target_container').length == false && _target.t.wrapInner('<span class="target_container"></span>'); 
			// _target.c = _target.t.find('.target_container');

			// function targetClear(){
			// 	_target.t.attr('style','').prop('style','');
			// 	_target.c.attr('style','').prop('style','');
			// }
			// targetClear();

			// var _parent = {};
			// if(_d.parent === false){
			// 	_parent.t = _target.t.parent();
			// 	_parent.top = _parent.t.offset().top
			// }
			// _parent.h = _parent.t.height();

			// var _win = {};
			// _win.t = $(window);

			// _target.t.css({'position':'relative'});

			// $(window).bind('scroll','scrollSticky',function(){
			// 	_win.h = _win.t.height();
			// 	_win.w = _win.t.width();
			// 	_win.top = _win.t.scrollTop() + $('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header, header.navbar .main-uninav-header').height();
			// 	if(_win.w > 960){
			// 		targetClear();

			// 		_target.th = _target.top + _target.h;
			// 		_win.th = (_win.top + _win.h) - $('header.uninav-push .header-fixed').height();
			// 		if(_win.th > _target.th){
			// 			_target.t.css({
			// 				height: _parent.h
			// 			})
			// 			_target.c.css({
			// 				position:'fixed',
			// 				bottom:'0px',
			// 				left:_target.l,
			// 				width:_target.w
			// 			})
			// 		}

			// 		console.log('scroll',_target,_parent,_win)
			// 	}
			// 	else {
			// 		targetClear();
			// 	}
			// });
		}
		PushStickyKit();

		var StarCinemaSticky = function StarCinemaSticky() {
            var rs = $('aside.push-container-right');
            var ls = $('.push-container-scroll > .push-container-left');

            function SCSticky2() {

                if ($('.sc-sticky-container').length == false) {
                    ls.wrapInner('<div class="sc-sticky-container">')
                    rs.wrapInner('<div class="sc-sticky-container">')
                }

                var wTop = $(window).scrollTop() + $(window).height();

                if ($(window).width() > 960) {
                    $('.sc-sticky-container').prop('style', '').attr('style', '');
                    $('.sc-sticky-container').data('targetP', false).data('targetW', false);

                    function stickyness(rs, ls) {
                        var v = $('.sc-sticky-container', rs);

                        if (v.height() > $(window).height()) {
                            var targetFollowedSticky = $('.sc-sticky-container', ls);;
                            var targetFollowedStickyT = targetFollowedSticky.offset().top + targetFollowedSticky.outerHeight();

                            //save element position in data
                            if (v.data('targetP') == false || typeof v.data('targetP') == 'undefined') v.data('targetP', v.offset().top + v.height());
                            var targetP = v.data('targetP');
                            //save element width in data
                            if (v.data('targetW') == false || typeof v.data('targetW') == 'undefined') v.data('targetW', v.outerWidth());
                            var targetW = v.data('targetW');

                            var bottom = 0;

                            if (wTop > targetFollowedStickyT) {
                                bottom = wTop - targetFollowedStickyT;
                            }

                            if (wTop > targetP) {
                                v.css({
                                    'position': 'fixed',
                                    'bottom': bottom,
                                    'left': $(v).offset().left,
                                    'width': targetW
                                });
                            } else {
                                $(v).attr('style', '').prop('style', '');
                            }
                        } else {
                            var wTop2 = $(window).scrollTop();

                            var targetFollowedSticky = $('.sc-sticky-container', ls);;
                            var targetFollowedStickyT = targetFollowedSticky.offset().top + targetFollowedSticky.outerHeight();

                            //save element position in data
                            if (v.data('targetP') == false || typeof v.data('targetP') == 'undefined') v.data('targetP', v.offset().top);
                            var targetP = v.data('targetP');
                            //save element width in data
                            if (v.data('targetW') == false || typeof v.data('targetW') == 'undefined') v.data('targetW', v.outerWidth());
                            var targetW = v.data('targetW');

                            var top = 50;

                            if (wTop > targetFollowedStickyT) {
                                top = wTop - targetFollowedStickyT;
                                top = -top;
                            }
                            if (wTop > targetP && wTop2 > targetP) {
                                v.css({
                                    'position': 'fixed',
                                    'top': top,
                                    'left': $(v).offset().left,
                                    'width': targetW
                                });
                            } else {
                                $(v).attr('style', '').prop('style', '');
                            }
                        }
                    }

                    if (rs.height() < ls.height()) {
                        stickyness(rs, ls);
                    } else {
                        stickyness(ls, rs);
                    }
                } else {
                    $('.sc-sticky-container').attr('style', '').prop('style', '');
                }
            };

            if (rs.length && ls.length) {
                SCSticky2();
                var _timer;
                var _timerT = 0;
                _timer = setInterval(function() {
                    if (_timerT < 10) {
                        SCSticky2()
                    }
                    _timerT++;
                }, 200);

                $(window)
                    .scroll(function() {
                        SCSticky2();
                    })
                    .resize(function() {
                        SCSticky2();
                    });
            }
        };
		StarCinemaSticky()

		//ASPECT-RATIOS
		var PushAspectRatios = function PushAspectRatios(data){
			return false;

			var conf = $.extend({
				// target: $('main.main#inner-pages .contents article #article-img, main.main#inner-pages .contents article #article-video, main.main#landing-pages .contents article #article-img')
				target: $('main.main#inner-pages .contents article #article-img, main.main#inner-pages .contents article #article-video')
			},data);
			var _ipaiT = conf.target;
			if(_ipaiT.hasClass('fixed') == false){
				var __img = _ipaiT.find('img');
				var _img = __img.attr('src');

				if(__img.length){
					loadImage(_img, function(err, img){
						if (err) throw err;
						// console.log(img.width, img.height);
						_ipaiT.addClass('fixed').append($('<span class="after">').css({'padding-top':(img.height / img.width * 100) + '%'}));
					});
					// console.log(baseUrl + _img)
					// loadImage(baseUrl + _img)
					// .then(function (img) {
					// 	// ctx.drawImage(img, 0, 0, 10, 10);
					// 	console.log(img)
					// })
					// .catch(function () {
					// 	console.error('Image failed to load :(');
					// });
				}
				else _ipaiT.addClass('fixed').addClass('skipfixed');
			}
		}
		PushAspectRatios();

		var PushBreadCrumbs = function PushBreadCrumbs(d) {
			var conf = $.extend({
				target: false
			},d);
			var target;

			if(conf.target == false) target = $('.push-breadcrumbs li:last-child span');
			else{
				target = conf.target.find('.push-breadcrumbs li:last-child span');
			}

			if($('.push-breadcrumbs li:first-child').find('a').length){
				th = $('.push-breadcrumbs li:first-child a').outerHeight();
			}
			else {
				th = $('.push-breadcrumbs li:first-child span').outerHeight();
			}

			target.dotdotdot({
				watch: true,
				height: th + 5
			});
		}
		PushBreadCrumbs();

		function CallbackSNS(target,nodeID,type,sns){
			if(nodeID == false && target != false && typeof target.data('nodeid') != 'undefined'){
				nodeID = target.data('nodeid');
			}
			if(type == false && target != false && typeof target.data('type') != 'undefined'){
				type = target.data('type');
			}
			if(typeof _InsertUpdatePageShareCount == 'function') _InsertUpdatePageShareCount(nodeID,type,sns);
			else {
				// function InsertUpdatePageShareCount(nodeID,type,sns) {
				// 	console.log(nodeID, type, sns);
				// }
				WebService.InsertUpdatePageShareCount(nodeID,type,sns);
			}
		}

		window.twitterClick = function twitterClick(e) {
			CallbackSNS($(e),false,false,2);
		};

		function twitterAddFunction(){
			// var conf = $.extend({
			// 	target:false
			// },data);

			// if(conf.target === false) return false;
			// else v = conf.target;

			$(document.body).on('click','.icon-tw',function(e){
				var v = e.currentTarget;
				var twttrURL = '';

				var _currentUrl = '';
				if(typeof $(v).data('url') !== 'undefined') _currentUrl = $(v).data('url');
				else _currentUrl = window.location.href;
				if($(e.currentTarget).parents('.slick-slide').first().length){
					_currentUrl += $(e.currentTarget).parents('.slick-slide').first().data('href');
				}

				var _text = ''
				if(typeof $(v).data('text') !== 'undefined') _text = $(v).data('text');
				else if(typeof $(v).data('title') !== 'undefined') _text = $(v).data('title');
				else if($(v).parents('#landing-pages').first().length) {
					_text = $(v).parents('article').first().find('#title').first().text();
				}
				else if($(v).parents('#inner-pages').first().length) {
					_text = $(v).parents('article').first().find('#title').first().text();						
				}

				var data = '';
				if(typeof $(v).data('nodeid') != 'undefined'){
					data += 'data-nodeid="'+$(v).data('nodeid')+'"';
				}
				if(typeof $(v).data('type') != 'undefined'){
					data += ' data-type="'+$(v).data('type')+'"';
				}

				//URL
				twttrURL += "https://twitter.com/intent/tweet?source=" + encodeURI(_currentUrl);
				twttrURL += _text ? '&text=' + encodeURIComponent($.trim(_text)) : '';
				twttrURL += '&url=' + encodeURI(_currentUrl);
				twttrURL += '&via=Push_Alerts';

				twitterClick(this);

				var w = 602;
				var h = 440;
				var left = (screen.width / 2) - (w / 2);
				var top = (screen.height / 2) - (h / 2);

				window.open(twttrURL,'feedDialog', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
			});

			// $(v).data('init',true);
		}
		twitterAddFunction();

		if(typeof InsertUpdatePageShareCount == 'function') var _InsertUpdatePageShareCount = InsertUpdatePageShareCount;
		var PushSocialMedia = function PushSocialMedia() {

			//GPLUS
			$('.icon-gplus').each(function(k,v){
				if(typeof $(v).data('init') == 'undefined') {
					var _currentUrl = '';
					if(typeof $(v).data('url') !== 'undefined') _currentUrl = $(v).data('url');
					else _currentUrl = window.location.href;

					// var data = '';
					// if(typeof $(v).data('nodeid') != 'undefined'){
					// 	data += 'data-nodeid="'+$(v).data('nodeid')+'"';
					// }
					// if(typeof $(v).data('type') != 'undefined'){
					// 	data += ' data-type="'+$(v).data('type')+'"';
					// }

					// $(v).wrap('<a href="https://plus.google.com/share?url='+_currentUrl+'" '+data+' onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');gplusClick(this);return false;">');
					$(v).click(function(e){
						var w = 400;
						var h = 500;
						var left = (screen.width / 2) - (w / 2);
						var top = (screen.height / 2) - (h / 2);

						window.open('https://plus.google.com/share?url='+_currentUrl,'feedDialog', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

						CallbackSNS($(e.currentTarget),false,false,3);
					});
					$(v).data('init',true);
				}
			});
			// window.gplusClick = function gplusClick(e){
			// 	CallbackSNS($(e),false,false,3);
			// }
			//TWITTER
			// $('.icon-tw').each(function(k, v){
			// 	if(typeof $(v).data('init') == 'undefined') {
			// 		twitterAddFunction({
			// 			target:v
			// 		});
			// 	}
			// });
			//FACEBOOK
			$('body').off('click','.icon-fb').on('click','.icon-fb',function(e){
				var _targetFb = $(e.currentTarget);
				var _targetParent = _targetFb.parents('article').first();
				var _link = typeof $(e.currentTarget).data('url') !== 'undefined' ? $(e.currentTarget).data('url') : window.location.href;
				if($(e.currentTarget).parents('.slick-slide').first().length){
					_link += $(e.currentTarget).parents('.slick-slide').first().data('href');
				}
				var _image = typeof _targetFb.data('image') == 'undefined' ? false : _targetFb.data('image');
				// if(_image == false && $('#article-img img',_targetParent).length){
				// 	_image = $('#article-img img',_targetParent).attr('src');
				// }
				var _title = typeof _targetFb.data('title') == 'undefined' ? false : _targetFb.data('title');
				// if(_title == false && $('#title',_targetParent).length) {
				// 	_title = $.trim($('#title',_targetParent).text());
				// }
				var _description = typeof _targetFb.data('description') == 'undefined' ? '' : _targetFb.data('description');
				// if(_description == false && $('#details',_targetParent).length){
				// 	_description = $.trim($('#details',_targetParent).text());
				// }
				if(_image !== false && _title !== false && true == false){
					// var url = 'https://www.facebook.com/dialog/feed?app_id=' + (typeof _FB_ID !== false ? _FB_ID : '') +
          //                   '&link=' + encodeURIComponent(_link) +
          //                   (_image ? '&picture=' + encodeURIComponent(_image) : '') +
          //                   '&name=' + encodeURIComponent(_title) +
          //                   '&description=' + encodeURIComponent(_description) + 
          //                   '&display=popup';

					// var w = 602;
					// var h = 369;
					// var left = (screen.width / 2) - (w / 2);
					// var top = (screen.height / 2) - (h / 2);

					// window.open(url,'feedDialog', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
					// FB.ui({
					// 	method: 'feed',
					// 	link: _link,
					// 	name: _title,
					// 	description: _description,
					// 	picture: _image,
					// }, function(response){
					// 	if (response && !response.error_message) {
					// 		CallbackSNS($(e.currentTarget),false,false,1);
					// 	}
					// });
					FB.ui({
						method: 'share_open_graph',
						action_type: 'og.shares',
						action_properties: JSON.stringify({
							object: {
									'og:url': _link, // your url to share
									'og:title': _title,
									'og:description': _description,
									'og:image': _image
								}
						})
					}, function(response){
						console.log(response);
						CallbackSNS($(e.currentTarget),false,false,1);
					});
				}
				else{
					FB.ui({
						method: 'share',
						display: 'popup',
						href: _link,
						appId: (typeof _FB_ID !== false ? _FB_ID : '')
					}, function(response) {
						if (response && !response.error_message) {
							CallbackSNS($(e.currentTarget),false,false,1);
						}
					});
				}
			});
		}
		PushSocialMedia();

		//SELECTS
		var PushSelects = function PushSelects(){
			$('select').each(function(k,v){
				$(v).select2({
					// placeholder:_placeholder,
					minimumResultsForSearch: Infinity,
				});
			})
		}
		PushSelects();

		var PushAds = function PushAds(){
			// var _p = $('[data-crowdynews-widget="ABSCBN1_vertical-widget"]');
			// var _l = $('.push-alert > div:nth-child(3)');
			var _l = $('aside .landscape');
			// var _p = $('.push-alert > div:nth-child(1)');
			var _p = $('aside .portrait');
			if($(window).width() < 960 && $(window).width() > 568) {
				_p.hide();
				_l.show();
			}
			else {
				_p.show();
				_l.hide();
			}
		}
		setInterval(function(){
			PushAds()
		},200)

		// setInterval(function(){
		// 	PushAds();
		// },300);

		//ORIENTATION CHANGE
		$(window).on('orientationchange', function(){
			// PushStickyKit();
			PushEllipsis();
			PushSelects();
			PushBreadCrumbs();
			PushSkinning();
			PushHTMLFixes({
				pushArticlesInfoFixes: true,
				searchMinHFix:true
			});
			PushSkinningFollow({reset:true});
			// PushAds();
		});
		//RESIZE CHANGE
		$(window).on('resize', function(){
			// PushStickyKit();
			PushEllipsis();
			PushSelects();
			PushBreadCrumbs();
			PushSkinning();
			PushHTMLFixes({
				pushArticlesInfoFixes: true,
				searchMinHFix:true
			});
			PushSkinningFollow({reset:true});
			// PushAds();
		});
		//Interval
		setInterval(function(){
			$('img:not([srcset])').each(function(k,v){
				typeof $(v).data('initImg') == 'undefined' && loadImage($(v).attr('src'),function(err,img){
					if(err){
						$(v).hide();
						$(v).data('initImg',true);
					}
				});
			});
		},500);
		var PushSkinningFollowB = false;
		var PushSkinningFollow = function PushSkinningFollow(d){
			return false;
			
			var conf = $.extend({
				reset: false
			},d);
			if(conf.reset === true) PushSkinningFollowB = false;

			//Remove this feature on ios
			// if(browser.name == 'ios') return false;

			var _sHome = {name:'shome'};
			_sHome.t = $('main.main#homepage .contents #push-skinning-container.with-skin');
			if(_sHome.t.length){
				_sHome.top = _sHome.t.offset().top;
				var _w = {name:'w'};
				_w.h = $(window).height();
				_w.top = $(window).scrollTop() + $(window).height();
				_w.top2 = $(window).scrollTop() + $('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header').height();
				var _psm = {name:'psm'};
				_psm.t = $('#push-skinning-mobile',_sHome.t);
				_psm.top = PushSkinningFollowB = (PushSkinningFollowB == false ? _psm.t.offset().top : PushSkinningFollowB);
				_psm.b = PushSkinningFollowB = (PushSkinningFollowB == false ? _psm.top + _psm.t.height() : PushSkinningFollowB);
				_psm.h = _psm.t.height();
				var _f = {name:'f'};
				_f.t = $('footer');
				_f.h = _f.t.height();
				_f.top = _f.t.offset().top;

				if($(window).width() > 960){
					if(_psm.h > _w.h){
						if(_w.top >= _f.top){
							_psm.t.css({
								'background-image':'url('+$('source[media="(min-width: 960px)"]',_psm.t).attr('srcset')+')',
								position:'fixed',
								top:'inherit',
								bottom:_f.h
							})
						}
						else if(_w.top2 > _psm.top){
							_psm.t.css({
								'background-image':'url('+$('source[media="(min-width: 960px)"]',_psm.t).attr('srcset')+')',
								position:'fixed',
								top:$('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header').height(),
								bottom:'inherit'
							});
						}
						else {
							_psm.t.css({
								'background-image':'url('+$('source[media="(min-width: 960px)"]',_psm.t).attr('srcset')+')',
								position:'absolute',
								top:0,
								bottom:'inherit'
							});
						}
					}
					else{
						if(_w.top2 > _psm.top){
							_psm.t.css({
								'background-image':'url('+$('source[media="(min-width: 960px)"]',_psm.t).attr('srcset')+')',
								position:'fixed',
								top:$('header.uninav-push .header-fixed').height() + $('header.uninav-header .main-uninav-header').height(),
								bottom:'inherit'
							});
						}
						else {
							_psm.t.css({
								'background-image':'url('+$('source[media="(min-width: 960px)"]',_psm.t).attr('srcset')+')',
								position:'absolute',
								top:0,
								bottom:'inherit'
							});
						}
					}
				}
				else{
					_psm.t.attr('style','').prop('style','');
				}
			}
		}
		//WINDOW SCROLL
		$(window).scroll(function(){
			PushSkinningFollow();
		});
		//TOOLTIPSTER
		$('.tooltip').tooltipster({
			theme:'tooltipster-shadow',
			interactive:true,
			side:'bottom',
			selfDestruction:false,
			repositionOnScroll:true,
			distance:5,
			animation:'grow',
			timer:'50000',
			trackOrigin:true,
			trackTooltip:true,
			trigger:'custom',
			delay:500,
			triggerOpen: {
				mouseenter: true,
			},
			triggerClose: {
				click:true,
				scroll:true,
				touchleave:true,
				tap:true,
				mouseleave:true
			},
			functionBefore: function(){
				$('.tooltip').tooltipster('close');
			},
			functionReady: function(instance, helper){
				var _tooltip = require('pug-loader!./../_pug/partials/_tooltip.pug');

				var _ttc = $('.tooltip_content',helper.tooltip);
				var _ttcul = $('.tooltip_content .push-articles ul',helper.tooltip);
				var _ttcid = _ttc.attr('id');

				function ellipTitle(){
					var _tt = $('.tooltip_content .push-articles ul li .push-articles-info-container .push-articles-info #title-container');
					_tt.each(function(k,v){
						$(v).find('#title').dotdotdot({
							height: $(v).height(),
							watch: 'window'
						});
					});
				}

				function processData(data,data2){
					var _sett = $.extend({
						type:false
					},data2);
					_ttcul.empty();
					$.each(data,function(k,v){
						_ttcul.append(_tooltip({
							data:{
								link:v.Url,
								img:v.IsMigrated === true || v.IsMigrated === 'True' ? v.Image : v.Image120,
								title:v.Title,
								date:v.RelativeDate,
								type:_sett.type
							}
						}));
					});
					ellipTitle();
					$(helper.tooltip).find('.tooltip_content').data('inits',true).addClass('tooltip_ready');
				}

				if(typeof $(helper.tooltip).find('.tooltip_content').data('inits') == 'undefined'){
					if(typeof WebService == 'undefined'){
						var _ajax = $.ajax({
							url:'theme/scripts/json/tooltipTest.json',
							dataType:'json',
							type:'get'
						});
						_ajax.success(function(data){
							processData(data,{type:'photo'});
						});
					}
					else {
						if(_ttcid == 'videos'){
							// WebService.LoadDyanamicPageTypes(null, "video", "newest", 0, 5,function(data){
							WebService.LoadDyanamicPageTypes("video", function(data){
								processData(data,{type:'video'});
							});
						}
						if(_ttcid == 'photos'){
							// WebService.LoadDyanamicPageTypes(null, "photo", "newest", 0, 5,function(data){
							WebService.LoadDyanamicPageTypes("photo", function(data){
								processData(data,{type:'photo'});
							});
						}
						if(_ttcid == 'fresh-scoops'){
							// WebService.LoadDyanamicPageTypes(null, "article", "newest", 0, 5,function(data){
							WebService.LoadDyanamicPageTypes("article", function(data){
								processData(data);
							});
						}
					}
				} else {
					ellipTitle();
				}
			},
			functionPosition: function(instance, helper, position){
				var _ww = $(window).width();
				var _w = $(window).width();
				var _h = $(window).height();
				_ww = _ww <= 1280 ? _ww : 1280;
				var _l = _w <= 1280 ? 0 : ((_w - 1280) / 2); 
				// console.log(position)
				position.size.height = (_h / 2) < 250 ? (_h / 2) : 250;
				position.size.width = _ww;
				position.coord.left = _l;
				return position;
			}
		});
		var PushInitComments2 = window.PushInitComments2 = function PushInitComments2(params) {
			var conf = $.extend({
				categoryID: 'Push',
				streamID: "123456UNIQUEID",
				version: 2,
				containerID: 'commentsDiv-123456UNIQUEID',
				cid: '',
				enabledShareProviders: 'facebook',
				disabledShareProviders: 'facebook',
				useSiteLogin: true,
				onSiteLoginClicked: function () {
					var goLogin = confirm('You must login first. Do you want to go to login page now.');
					if (goLogin) {
						window.location.href = 'http://staging-kapamilyaaccounts.abs-cbn.com/login';
					}
				},
				onCommentSubmitted: function(d){
					var _cnt = $('#'+params.containerID).parents('article').first().find('#counts');
					var __cnt = parseInt(_cnt.text());
					_cnt.text(__cnt + 1);
				}
			},params)
			// $('#'+conf.containerID).empty();
			function count(response){
				if (response.errorCode == 0) {            
					if (null!=response.comments && response.comments.length>0) {       
						// var msg = "The first 3 comments are:\n\n";  
						// for (var index in response.comments) {    
						// 	msg += "Comment " + index + ": " + response.comments[index].commentText;
						// 	msg += "\n  Written by: " + response.comments[index].sender.name + "\n\n";
						// }
						// console.log(msg);

						// $('.sca-container.inner-pages.inner-pages-article').last().find('div#scm-social-gigya-comments-count #scm-social-gigya-comments-count-fixer').text(response.commentCount);
						$('#'+conf.containerID).parents('li').first().find('#responses-shares .counts').text(response.commentCount);
					}
					else {
						$('#'+conf.containerID).parents('li').first().find('#responses-shares .counts').text('0');
						// console.log('No comments were returned');
					}
				} else {
					console.log('Error :' + response.errorMessage);
				}
				// console.log(response);
			}
			var _sett = $.extend({
				callback: count
			}, conf);
			gigya.comments.getComments(_sett);
		}
		var PushInitComments = window.PushInitComments = function PushInitComments(params) {

			var conf = $.extend({
				categoryID: 'Push',
				streamID: "123456UNIQUEID",
				version: 2,
				containerID: 'commentsDiv-123456UNIQUEID',
				cid: '',
				enabledShareProviders: 'facebook',
				disabledShareProviders: 'facebook',
				useSiteLogin: true,
				onSiteLoginClicked: function () {
					var goLogin = confirm('You must login first. Do you want to go to login page now.');
					if (goLogin) {
						window.location.href = 'http://staging-kapamilyaaccounts.abs-cbn.com/login';
					}
				},
				onCommentSubmitted: function(d){
					var _cnt = $('#'+params.containerID).parents('article').first().find('#counts');
					var __cnt = parseInt(_cnt.text());
					_cnt.text(__cnt + 1);
				}
			},params)

			function count(response){
				if (response.errorCode == 0) {            
					if (null!=response.comments && response.comments.length>0) {       
						// var msg = "The first 3 comments are:\n\n";  
						// for (var index in response.comments) {    
						// 	msg += "Comment " + index + ": " + response.comments[index].commentText;
						// 	msg += "\n  Written by: " + response.comments[index].sender.name + "\n\n";
						// }
						// console.log(msg);

						// $('.sca-container.inner-pages.inner-pages-article').last().find('div#scm-social-gigya-comments-count #scm-social-gigya-comments-count-fixer').text(response.commentCount);
						$('#'+conf.containerID).parents('article').first().find('#counts').text(response.commentCount);
					}
					else {
						$('#'+conf.containerID).parents('article').first().find('#counts').text('0');
						// console.log('No comments were returned');
					}
				} else {
					console.log('Error :' + response.errorMessage);
				}
			}

			console.info('Initiating comments.', conf);
			typeof gigya != 'undefined' && gigya.comments.showCommentsUI(conf);
			var _sett = $.extend({
				callback: count
			}, conf);
			typeof gigya != 'undefined' && gigya.comments.getComments(_sett);
		};
	}

	return {
		init: init
	};
}());

jQuery(document).ready(function($) { ABSCBN.init(); });

function getPathFromUrl(url) {
  return url.split(/[?#]/)[0];
}

//FACEBOOK SCRIPT START
window.fbAsyncInit = function() {
    FB.init({
        appId: (typeof _FB_ID !== false ? _FB_ID : ''), // TODO: Change to production id
        xfbml: true,
        version: 'v2.8'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
//FACEBOOK SCRIPT END
//TWITTER SCRIPT START
window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
        t._e.push(f);
    };

    return t;
}(document, "script", "twitter-wjs"));
//TWITTER SCRIPT END
